use crate::packages::Dependency;
use crate::Packages;
use std::collections::HashSet;

use rpkg::debversion;
use rpkg::debversion::DebianVersionNum;
use rpkg::debversion::VersionRelation::GreaterOrEqual;

impl Packages {
    /// Computes a solution for the transitive dependencies of package_name; when there is a choice A | B | C,
    /// chooses the first option A. Returns a Vec<i32> of package numbers.
    ///
    /// Note: does not consider which packages are installed.
    pub fn transitive_dep_solution(&self, package_name: &str) -> Vec<i32> {
        if !self.package_exists(package_name) {
            return vec![];
        }

        let deps: &Vec<Dependency> = &*self
            .dependencies
            .get(self.get_package_num(package_name))
            .unwrap();
        let mut dependency_set = vec![];
        let mut worklist = vec![deps];

        // implement worklist
        while !worklist.is_empty() {
            let deps = worklist.pop().unwrap();
            for dep in deps {
                let pkg = &dep[0];
                if !dependency_set.contains(&pkg.package_num) {
                    worklist.push(self.dependencies.get(&pkg.package_num).unwrap());
                    dependency_set.push(pkg.package_num);
                }
            }
        }

        return dependency_set;
    }

    /// Computes a set of packages that need to be installed to satisfy package_name's deps given the current installed packages.
    /// When a dependency A | B | C is unsatisfied, there are two possible cases:
    ///   (1) there are no versions of A, B, or C installed; pick the alternative with the highest version number (yes, compare apples and oranges).
    ///   (2) at least one of A, B, or C is installed (say A, B), but with the wrong version; of the installed packages (A, B), pick the one with the highest version number.
    pub fn compute_how_to_install(&self, package_name: &str) -> Vec<i32> {
        if !self.package_exists(package_name) {
            return vec![];
        }
        let mut dependencies_to_add: Vec<i32> = vec![];

        // implement more sophisticated worklist
        let deps: &Vec<Dependency> = &*self
            .dependencies
            .get(self.get_package_num(package_name))
            .unwrap();
        let mut worklist = vec![deps];
        let mut deps_done: HashSet<String> = HashSet::new(); // Do not need to do duplicate work.
        let mut deps_set: HashSet<i32> = HashSet::new(); // Do not need to add duplicate dependencies.

        // implement worklist
        while !worklist.is_empty() {
            let deps = worklist.pop().unwrap();
            for dep in deps {
                // Don't need to do duplicate work.
                if deps_done.contains(&self.dep2str(dep)) {
                    continue;
                }

                // Mark this dependency as done.
                deps_done.insert(self.dep2str(dep));

                // Check if any alternative is installed.
                let mut need_to_install = true;
                let mut installed_wrong_version = false;
                let mut highest_version_ref = &DebianVersionNum {
                    epoch: String::from("0"),
                    upstream: String::from("0"),
                    debian: String::from("0"),
                };
                let mut verion_to_install = -1;
                for pkg in dep {
                    // If this package is installed.
                    if let Some(pkg_version) =
                        self.get_installed_debver(self.get_package_name(pkg.package_num))
                    {
                        // If version relation is specified, check if the installed version satisfies the relation.
                        if let Some(version_info) = &pkg.rel_version {
                            let (op, version_req) = (&version_info.0, &version_info.1);
                            if debversion::cmp_debversion_with_op(
                                op,
                                &pkg_version,
                                &version_req.parse::<debversion::DebianVersionNum>().unwrap(),
                            ) {
                                // The installed version satisfies the relation.
                                need_to_install = false;
                                break;
                            } else {
                                // Installed wrong version.
                                installed_wrong_version = true;
                                // Find the highest possible version from this pkg that also satisfies the relation.
                                if let Some(avail_deb_ver) = self
                                    .get_available_debver(self.get_package_name(pkg.package_num))
                                {
                                    if debversion::cmp_debversion_with_op(
                                        op,
                                        &avail_deb_ver,
                                        &version_req
                                            .parse::<debversion::DebianVersionNum>()
                                            .unwrap(),
                                    ) && debversion::cmp_debversion_with_op(
                                        &GreaterOrEqual,
                                        &avail_deb_ver,
                                        highest_version_ref,
                                    ) {
                                        // Find the highest possible version from this pkg.
                                        highest_version_ref = avail_deb_ver;
                                        verion_to_install = pkg.package_num;
                                    }
                                }
                            }
                        }
                    }
                }

                // No need to install any pky for this dependency.
                if !need_to_install {
                    continue;
                }

                // Not installed, find the highest possible version from all alternatives.
                if !installed_wrong_version {
                    for pkg in dep {
                        // Check if an available version satisfies the relation.
                        if let Some(version_info) = &pkg.rel_version {
                            // Find the highest possible version from this pkg that also satisfies the relation.
                            let (op, version_req) = (&version_info.0, &version_info.1);
                            if let Some(avail_deb_ver) =
                                self.get_available_debver(self.get_package_name(pkg.package_num))
                            {
                                if debversion::cmp_debversion_with_op(
                                    op,
                                    &avail_deb_ver,
                                    &version_req.parse::<debversion::DebianVersionNum>().unwrap(),
                                ) && debversion::cmp_debversion_with_op(
                                    &GreaterOrEqual,
                                    &avail_deb_ver,
                                    highest_version_ref,
                                ) {
                                    // Find the highest possible version from this pkg.
                                    highest_version_ref = avail_deb_ver;
                                    verion_to_install = pkg.package_num;
                                }
                            }
                        }
                    }
                }

                // Nothing can satisfy the dependency, do not install anything.
                if verion_to_install == -1 || deps_set.contains(&verion_to_install) {
                    continue;
                }

                // Add the highest possible version to the storage.
                dependencies_to_add.push(verion_to_install);
                // deps_set.insert(verion_to_install);
                worklist.push(self.dependencies.get(&verion_to_install).unwrap());
            }
        }
        return dependencies_to_add;
    }
}
