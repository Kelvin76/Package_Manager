use crate::packages::Dependency;
use crate::Packages;
use rpkg::debversion::{self};

impl Packages {
    /// Gets the dependencies of package_name, and prints out whether they are satisfied (and by which library/version) or not.
    pub fn deps_available(&self, package_name: &str) {
        if !self.package_exists(package_name) {
            println!("no such package {}", package_name);
            return;
        }
        println!("Package {}:", package_name);
        for deps in self
            .dependencies
            .get(self.get_package_num(package_name))
            .unwrap()
        {
            self.dep_is_satisfied(deps);
        }
    }

    /// Returns Some(package) which satisfies dependency dd, or None if not satisfied.
    pub fn dep_is_satisfied(&self, dd: &Dependency) -> Option<&str> {
        let mut dependency_name = String::from("");
        let mut satisfaction_printout = String::from("-> not satisfied");
        let mut satisfied = false;
        let mut res: Option<&str> = None;

        for pkg in dd {
            if dependency_name.len() > 0 {
                dependency_name.push_str(&" | ");
            }

            let pkg_num = pkg.package_num;
            let name = self.get_package_name(pkg_num);

            // If the dependency is satisfied by an installed package, print that and return.
            if let Some(version_info) = &pkg.rel_version {
                let (op, version) = (&version_info.0, &version_info.1);
                dependency_name.push_str(&format!("{} ({} {})", name, op, version));
                if satisfied {
                    continue;
                }

                if let Some(pkg) = self.installed_debvers.get(&pkg_num) {
                    if debversion::cmp_debversion_with_op(
                        op,
                        &pkg,
                        &version.parse::<debversion::DebianVersionNum>().unwrap(),
                    ) {
                        satisfaction_printout = String::from(format!(
                            "+ {} satisfied by installed version {}",
                            name, pkg
                        ));
                        res = Some(name);
                        satisfied = true;
                    }
                }
            } else {
                dependency_name.push_str(name);
                if satisfied {
                    continue;
                }

                if let Some(pkg) = self.installed_debvers.get(&pkg_num) {
                    satisfaction_printout =
                        String::from(format!("+ {} satisfied by installed version {}", name, pkg));
                    res = Some(name);
                    satisfied = true;
                }
            }
        }

        if dependency_name.len() > 0 {
            println!("- dependency \"{dependency_name}\"");
            println!("{satisfaction_printout}");
        }
        return res;
    }
}
